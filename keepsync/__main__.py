import gkeepapi
import click
import json
import keyring
import sys
from pathlib import Path

@click.group()
def cli():
    pass

@cli.command('sync')
@click.option('-u', '--user', help='username for google')
@click.option('-t', '--token', help='pass token instead of checking keyring', default=None)
@click.option('-o', '--save-location', help='save location', default='~/keep_backup')
def sync( user, token, save_location ):
    "login and save stuff locally"
    keep = gkeepapi.Keep()
    if token is None:
        token = keyring.get_password('keepsync', user)

    try:
        keep.resume(user, token)
    except:
        print('Bad or expired token')
        sys.exit(1)

    # Store cache
    state = keep.dump()
    save_path = Path(save_location).expanduser().resolve()
    with save_path.open('w') as fh:
        json.dump(state, fh)
    print('Successfully persisted.')

@cli.command('login')
@click.option('-u', '--user', help='username for google', prompt='Username')
@click.option('-p', '--password', help='password for google', prompt='Password')
def login( user, password ):
    "login and generate a token which is then saved to the keyring"
    keep = gkeepapi.Keep()

    try:
        keep.login(user, password)
    except:
        print('Failed to login!')
        sys.exit(1)

    token = keep.getMasterToken()
    try:
        keyring.set_password('keepsync', user, token)
    except:
        print('Could not persist to keyring! Do you have a valid keyring installed?')
        sys.exit(1)

    print('Logged in and token saved.')

if __name__ == '__main__':
    cli()
