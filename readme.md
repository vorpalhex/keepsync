# keepsync

A small app to help you back up your google keep locally

## Usage

1. Login

`keepsync login --user myaddress@gmail.com --password mygmailpassword`

This will save a token to your keyring for future use.

2. Sync

`keepsync sync --user myaddress@gmail.com --save-location some/file/location`

This will grab a gkeepapi compatible JSON dump of everything in your Google keep.

## Commands

### Login

```
Usage: keepsync login [OPTIONS]

  login and generate a token which is then saved to the keyring

Options:
  -u, --user TEXT      username for google
  -p, --password TEXT  password for google
  --help               Show this message and exit.
```

### Sync

```
Usage: keepsync sync [OPTIONS]

  login and save stuff locally

Options:
  -u, --user TEXT           username for google
  -t, --token TEXT          pass token instead of checking keyring
  -o, --save-location TEXT  save location
  --help                    Show this message and exit.
```

If you don't supply a save location, it'll default to `~/keep_backup`.

## FAQ

### 2FA

If you use 2FA on your account (and you should!) you'll need to create and [use an app password](https://support.google.com/accounts/answer/185833?hl=en) to get your token.

### Could not persist to keyring!

If you get this error during the login process, it means python's `keyring` module failed to find a valid keyring. On Linux/BSD, make sure you have `gnome-keyring` available. On Windows... install Linux?
